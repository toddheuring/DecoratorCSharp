FROM microsoft/dotnet
WORKDIR /app

COPY ./ ./

RUN dotnet restore
CMD dotnet test